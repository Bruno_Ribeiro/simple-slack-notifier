import os
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from datetime import datetime, timedelta
import argparse
import time
import json
import re

# Initialize the Slack client

def alert_messages(arguments):
    try:
        # Convert start date to Unix timestamp
        has_more = True
        start_date = arguments["from_time"]

        while has_more == True:
            # Fetch messages from the channel
            response = arguments["client"].conversations_history(channel=arguments["channel_id"], oldest=start_date,limit=100)
            messages = response.get("messages", [])
            if str(messages) != "[]":
                start_date = messages[0]["ts"]
                has_more = response["has_more"]
                for message in messages:
                    matching = re.search(f"{arguments['message_regex']}", str(message).lower(), re.IGNORECASE)
                    if matching:
                        try:
                            # Start a conversation with the user
                            conversation = arguments["client"].conversations_open(users=[arguments["user_id"]])
                            channel_id = conversation["channel"]["id"]
                            content = message["text"]
                            timestamp = message["ts"]

                            # Convert Unix timestamp to UTC datetime
                            utc_time = datetime.utcfromtimestamp(float(timestamp))
                            
                            message_time = utc_time.strftime('%Y-%m-%d %H:%M:%S.%f')

                            # Post a message to the user
                            response = arguments["client"].chat_postMessage(
                                channel=channel_id,
                                text=f"Message recieved in: \n - Channel: {arguments['channel_name']}\n - Time: {message_time}\n - Content {content}"
                            )
                            print("Message sent: ", response["message"]["text"])

                        except SlackApiError as e:
                            print(f"Error sending message: {e.response['error']}")
                            print(message)

            else:
                    has_more = False

    except SlackApiError as e:
        print(f"Error fetching conversations: {e}")
        return {}

def main():
    # initialize slack cliente
    client = WebClient(token=os.getenv('SLACK_TOKEN'))
    # Get the Channels to look at
    with open('channels.json', 'r') as file:
        channels = json.load(file)
        
    time_interval=int(os.getenv('TIME'))
    user_id=os.getenv('USER_ID')
    regex=os.getenv('REGEX')

    print(time_interval)
    while(1):
        now = datetime.now()  
        ten_minutes_ago = now - timedelta(minutes=time_interval)
        epoch_time = ten_minutes_ago.timestamp()
        for channel in channels:
            arguments = {
                "channel_id": channels[channel],
                "channel_name": channel,
                "from_time": epoch_time,
                "user_id": user_id,
                "client": client,
                "message_regex": regex
            }
            alert_messages(arguments)
        time.sleep(60*time_interval)

if __name__ == "__main__":
    main()
