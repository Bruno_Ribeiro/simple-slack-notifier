docker_build:
	docker build -t slack_bot_messager:0.0.1 -f images/Dockerfile .

docker_run:
	docker run -d --env-file ./.env --name messager slack_bot_messager:0.0.1