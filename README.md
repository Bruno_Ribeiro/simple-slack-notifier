# Simple Slack Notifier

## Populate the secrets and variables

Populate your .env file in the root folder of the project like the following example:

**SLACK_TOKEN=<slack_token>** # Slack Bot that will read the messages and resend to the User_ID

**TIME=15** # Time in minutes for the bot to verify the channel.

**REGEX=eda** # Regex for the bot to match the messages that it will resend to the USER_ID

**USER_ID=<user_id>** # USER_ID of the user to resend the messages to

## Populate the channels to read

Populate your channels.json file in the root folder of the project like the following example:

{
    "#alerts-au-low-itf" : "<channel_id>", 
    "#alerts-aavs3" : "<channel_id>", 
    "#alerts-dish-lmc" : "<channel_id>", 
}

## Build the docker image

make docker_build

## Start the image

make docker_run